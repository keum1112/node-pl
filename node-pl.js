var path = require('path');
var fs = require('fs');

var root_path = '';
var extArr = ['.html', '.java', '.xml', '.jsp', '.js', '.css', '.xfdl', '.xjs'];

function getFiles(dir, files_) {
	files_ = files_ || [];
	var files = fs.readdirSync(dir);
	for(var i in files) {
		var name = dir + '/' + files[i];
		if(fs.statSync(name).isDirectory()) {
			getFiles(name, files_);
		} else {
			if(isDevFile(name)) {
				files_.push(name);
			}
		}
	}
	return files_;
}

function isDevFile(file) {
	var ext = path.extname(file);

	for(var i in extArr) {
		_ext = extArr[i];
		if(_ext === ext)		return true;
	}
	
	return false;
}

var devFiles = getFiles(root_path);

var stream = fs.createWriteStream('test.txt');
stream.once('open', function() {
	for(var i in devFiles) {
		var devFile = devFiles[i].replace(root_path, '');

		var _path = devFile.substr(0, devFile.lastIndexOf('/')+1);
		var _file = devFile.substr(devFile.lastIndexOf('/')+1, devFile.length);
		var _ext = path.extname(_file).replace('.', '');

		stream.write(_path + ", " + _file + " | " + _ext);
		stream.write("\n");
	}
	stream.end();		
});

